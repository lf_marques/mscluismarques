from __future__ import print_function
import sys
from pyspark import SparkContext

def gen_window(xi, n):
        (name,date,val), i = xi
        return [((i - offset,name), (date,val,1)) for offset in xrange(n)]



if __name__ == "__main__":

	WINDOW_SIZE = 20

	sc = SparkContext()

	lines = sc.textFile(sys.argv[1])
	dateValueMap = lines.map(lambda (line):(line.split(",",2)[0],line.split(",",2)[1],float(line.split(",",7)[5])))
	putIndex = dateValueMap.zipWithIndex()
	slidingWindow = putIndex.flatMap(lambda xi:gen_window(xi,WINDOW_SIZE))
	reduceRDD = slidingWindow.reduceByKey(lambda (date,val,count),(date2,val2,count2):(max(date,date2),val+val2,count+count2))
	filterRDD = reduceRDD.filter(lambda (key,(date,sum,count)): count == WINDOW_SIZE).cache()

	mapRDDMAE = filterRDD.map(lambda ((id,name),(date, sum, count)): ((name,date),((sum/float(count) - ((sum/float(count))*0.025)),(sum/float(count) + ((sum/float(count))*0.025)))))
	sortedRDDMAE = mapRDDMAE.sortByKey().cache()

    for x in mapRDDMAE.collect():

		with open('MAE.py', 'wb') as f:
			f.write(x)

	mapRDDSMA = filterRDD.map(lambda ((id,name),(date, sum, count)): ((name,date),(sum/float(count))))
	sortedRDDSMA = mapRDDSMA.sortByKey().cache()


    sortedRDDSMA.cache().collect()

	with open('SMA20.py', 'wb') as f:
		f.write(sortedRDDSMA.count())

	addMeanRDD = filterRDD.join(slidingWindow)
	newMapRDD = addMeanRDD.map(lambda (key,((date, sum, count), (oldDate, val, newCount))): (key,(date, ((sum/float(count))-val)**2, newCount )))
	newReduceRDD = newMapRDD.reduceByKey(lambda (date, sq, count1),(date2, sq2, count2):(max(date), sq+sq2, count1+count2)).cache()

    newReduceRDD.cache().collect()

	with open('BB.py', 'wb') as f:
		f.write(newReduceRDD.count())
