package thesis.msc.manager;

import java.io.*;
import java.util.*;

import thesis.msc.executer.FilesCreator;
import thesis.msc.executer.FilesCreatorSingle;
import thesis.msc.executer.PrintableFile;
import thesis.msc.executer.PrintableSingleFile;
import thesis.msc.executer.ExporterPython;
import thesis.msc.executer.ExporterScala;
import thesis.msc.executer.ExporterSinglePython;
import thesis.msc.executer.ExporterSingleScala;
import thesis.msc.graph.Edge;
import thesis.msc.graph.Graph;
import thesis.msc.graph.VertexComplex;
import thesis.msc.graph.VertexComplexOp;
import thesis.msc.importer.*;
import thesis.msc.optimizer.GroupOptimizer;
import thesis.msc.partitioner.LongestPartitioner;

public class Manager {

	private List<ImportedJob> importedJobs;
	private static final int SCALA = 1;
	private static final int PYTHON = 2;
	private int language;

	public Manager(String language) {

		if (language.equals("scala"))
			this.language = SCALA;
		else if (language.equals("python"))
			this.language = PYTHON;
		importedJobs = new LinkedList<ImportedJob>();
	}

	public void processJob(File f) throws FileNotFoundException, BadInputException {
		ImportedJob j = null;
		switch (language) {
		case SCALA:
			j = new ImportedJobScala(f);
			break;
		case PYTHON:
			j = new ImportedJobPython(f);
			break;
		default:
			throw new BadInputException();
		}

		importedJobs.add(j);
	}

	private Graph<VertexComplexOp, Edge> optimize() {
		return new GroupOptimizer().optimize(importedJobs);
	}

	public String print_optimized() {
		return optimize().toString();
	}

	private Graph<VertexComplex, Edge> partitionate(Graph<VertexComplexOp, Edge> optimizedGraph) {
		return new LongestPartitioner().partitionate(optimizedGraph);
	}

	public String print_partitionated() {
		return partitionate(optimize()).toString();
	}

	public void generateResult(File f, boolean isSingle) throws BadInputException {

		List<PrintableFile> list = null;
		switch (language) {
		case SCALA:
			if (isSingle) {
				List<PrintableSingleFile> newList = new FilesCreatorSingle(f, ".scala")
						.createFiles(partitionate(optimize()));
				for (PrintableSingleFile p : newList)
					new ExporterSingleScala(p).createFile();
			} else {
				list = new FilesCreator(f, ".scala").createFiles(partitionate(optimize()));
				for (PrintableFile p : list)
					new ExporterScala(p).createFile();
			}
			break;
		case PYTHON:
			if (isSingle) {
				List<PrintableSingleFile> newList = new FilesCreatorSingle(f, ".py")
						.createFiles(partitionate(optimize()));
				for (PrintableSingleFile p : newList)
					new ExporterSinglePython(p).createFile();
			} else {
				list = new FilesCreator(f, ".py").createFiles(partitionate(optimize()));
				for (PrintableFile p : list)
					new ExporterPython(p).createFile();
			}
			break;
		default:
			throw new BadInputException();
		}

	}

}
