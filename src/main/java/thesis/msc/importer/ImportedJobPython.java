package thesis.msc.importer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import thesis.msc.graph.*;
import thesis.msc.graph.info.*;

public class ImportedJobPython extends ImportedJobAbstract implements ImportedJob {

	public ImportedJobPython(File f) throws FileNotFoundException, BadInputException {
		super();
		Scanner file = new Scanner(f);

		readImports(file);

		readMethods(file);

		readVariables(file);

		String appName = readComputations(file, f.getName());

		readSufix(file, appName);
		
		this.job = f.getName();

		file.close();

	}

	private void readImports(Scanner file) throws BadInputException {
		String input = file.nextLine().trim();

		while (!input.equals("#@begin_imports")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("File should start with #@begin_imports");
		}

		input = file.nextLine().trim();

		while (!input.equals("#@end_imports")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			if (!input.startsWith("from") && !input.startsWith("import"))
				throw new BadInputException("imports should start either with from or import:" + input);

			ImportPython imp = new ImportPython(input);
			ImportPython other = (ImportPython) imports.get(imp.getFrom());
			if (other != null)
				other.mergeImports(imp);
			else
				imports.put(imp.getFrom(), imp);

			input = file.nextLine().trim();
		}

	}

	private void readMethods(Scanner file) throws BadInputException {
		String input = file.nextLine().trim();

		while (!input.equals("#@begin_methods")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("File should start with #@begin_methods");
		}

		input = file.nextLine().trim();

		while (!input.equals("#@end_methods")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			if (!input.startsWith("def"))
				throw new BadInputException("methods should start with def");

			List<String> result = new LinkedList<String>();
			
			while (true) {
				result.add(input);

				input = file.nextLine();
				if (input.startsWith("def") || input.startsWith("#@end_methods")) {
					Method met = new Method(result);
					if (methods.put(met.id(), met) != null)
						throw new BadInputException("repeated method.");
					break;
				}
			}
		}
	}

	private void readVariables(Scanner file) throws BadInputException {
		String input = file.nextLine().trim();

		while (!input.equals("#@begin_setup")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("File should start with #@begin_setup");
		}

		input = file.nextLine().trim();

		while (!input.contains("__name__") && !input.contains("__main")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("The setup should start with if __name__ == \"__main__\":" + input);
		}

		input = file.nextLine().trim();

		while (!input.equals("#@end_setup")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}

			if (!input.contains("="))
				throw new BadInputException("Only assignments can be done in the setup fase:" + input);

			Variable v = new Variable(input);
			variables.put(v.getId(), v);

			input = file.nextLine().trim();
		}
	}

	private String readComputations(Scanner file, String fileName) throws BadInputException {
		String input = file.nextLine().trim();

		while (!input.equals("#@begin_spark_prog")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("Program should start with #@begin_spark_prog");
		}

		input = file.nextLine().trim();

		while (!input.startsWith("sc")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("You should start by definiding the sc (SparkContext) first:" + input);
		}

		String appName = input.split("=")[2].replace("\"", "").replace(")", "").trim();

		VertexSingle root = new VertexSingleOp(DEFAULTWEIGHT, null, null, "sc", "SparkContext", null);
		computations = new GraphTree<VertexSingle, Edge>(root, Edge.class);

		HashMap<String, VertexSingle> parents = new HashMap<String, VertexSingle>(30);
		parents.put("sc", root);

		input = file.nextLine().trim();
		
		while (!input.equals("#@end_spark_prog")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			
			Fields f = extractFields(input);

			VertexSingle parent = parents.get(f.parentId);
			parent.notFinal();

			VertexSingle v = new VertexSingleOp(DEFAULTWEIGHT, parent, f.arg, f.id, f.method, fileName);

			computations.addVertex(v);

			parents.put(f.id, v);

			computations.addEdge(parent, v);
			
			input = file.nextLine().trim();
		}
		
		return appName;

	}

	private void readSufix(Scanner file, String appName) {
		List<String> result = new LinkedList<String>();
		while(file.hasNext())
			result.add(file.nextLine());
		try {
			this.sufix = new Sufix(result, appName, null);
		} catch (BadInputException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private Fields extractFields(String line) {
		String id;
		String parentId;
		String method;
		Arg arg;

		int tmpIndex = line.indexOf("=");
		id = line.substring(0, tmpIndex).trim();

		line = line.substring(tmpIndex + 1).trim();

		tmpIndex = line.indexOf(".");
		parentId = line.substring(0, tmpIndex).trim();

		line = line.substring(tmpIndex + 1).trim();

		tmpIndex = line.indexOf("(");
		method = line.substring(0, tmpIndex).trim();

		line = line.substring(tmpIndex + 1).trim();

		if (line.charAt(line.length() - 1) == ';')
			line = line.substring(0, line.length() - 2);
		else
			line = line.substring(0, line.length() - 1);

		arg = new ArgPython(line);

		return new Fields(id, parentId, method, arg);
	}

	

}
