package thesis.msc.importer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import thesis.msc.graph.Edge;
import thesis.msc.graph.GraphTree;
import thesis.msc.graph.VertexSingle;
import thesis.msc.graph.VertexSingleOp;
import thesis.msc.graph.info.Arg;
import thesis.msc.graph.info.ArgScala;
import thesis.msc.graph.info.ImportScala;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public class ImportedJobScala extends ImportedJobAbstract implements ImportedJob {

	public ImportedJobScala(File f) throws FileNotFoundException, BadInputException {
		super();
		Scanner file = new Scanner(f);

		readImports(file);

		String className = readMethods(file);

		readVariables(file);

		String appName = readComputations(file, f.getName());

		readSufix(file, appName, className);
		
		this.job = f.getName();

		file.close();

	}

	private void readSufix(Scanner file, String appName, String className) {
		List<String> result = new LinkedList<String>();
		while(file.hasNext())
			result.add(file.nextLine());
		for(int i = 1; i<=2; ){
			String line = result.get(result.size()-1).trim();
			if(line.equals("")|line.equals("}")){
				result.remove(result.size()-1);
				if(line.equals("}"))
					i++;
			}
			else if(line.equals("}}")){
				result.remove(result.size()-1);
				break;
			}
		}
		try {
			this.sufix = new Sufix(result, appName, className);
		} catch (BadInputException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String readComputations(Scanner file, String fileName) throws BadInputException {
		String input = file.nextLine().trim();

		while (!input.equals("//@begin_spark_prog")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("Program should start with //@begin_spark_prog");
		}

		input = file.nextLine().trim();

		while (!input.startsWith("val conf")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("You should start by definiding val conf = new SparkConf().setAppName(...):" + input);
		}

		String appName = input.split("\"")[1];
		
		input = file.nextLine().trim();
		while (!input.startsWith("val sc")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("You should define sc like this: val sc = new SparkContext(conf)" + input);
		}

		VertexSingle root = new VertexSingleOp(DEFAULTWEIGHT, null, null, "sc", "SparkContext", null);
		computations = new GraphTree<VertexSingle, Edge>(root, Edge.class);

		HashMap<String, VertexSingle> parents = new HashMap<String, VertexSingle>(30);
		parents.put("sc", root);

		input = file.nextLine().trim();
		
		while (!input.equals("//@end_spark_prog")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			
			Fields f = extractFields(input);

			VertexSingle parent = parents.get(f.parentId);
			parent.notFinal();

			VertexSingle v = new VertexSingleOp(DEFAULTWEIGHT, parent, f.arg, f.id, f.method, fileName);

			computations.addVertex(v);

			parents.put(f.id, v);

			computations.addEdge(parent, v);
			
			input = file.nextLine().trim();
		}
		
		return appName;
	}

	private Fields extractFields(String line) {
		String id;
		String parentId;
		String method;
		Arg arg;

		int tmpIndex = line.indexOf("=");
		id = line.substring("val".length(), tmpIndex).trim();

		line = line.substring(tmpIndex + 1).trim();

		tmpIndex = line.indexOf(".");
		parentId = line.substring(0, tmpIndex).trim();

		line = line.substring(tmpIndex + 1).trim();

		tmpIndex = line.indexOf("(");
		method = line.substring(0, tmpIndex).trim();

		line = line.substring(tmpIndex + 1).trim();

		if (line.charAt(line.length() - 1) == ';')
			line = line.substring(0, line.length() - 2);
		else
			line = line.substring(0, line.length() - 1);

		arg = new ArgScala(line);
		
		return new Fields(id, parentId, method, arg);
	}

	private void readVariables(Scanner file) throws BadInputException {
		String input = file.nextLine().trim();

		while (!input.equals("//@begin_setup")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("File should start with //@begin_setup");
		}

		input = file.nextLine().trim();

		while (!input.contains("def main(")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("The setup should start with def main" + input);
		}

		input = file.nextLine().trim();

		while (!input.equals("//@end_setup")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}

			if (!input.contains("="))
				throw new BadInputException("Only assignments can be done in the setup fase:" + input);

			Variable v = new Variable(input.substring("val".length()));
			variables.put(v.getId(), v);

			input = file.nextLine().trim();
		}
	}

	private String readMethods(Scanner file) throws BadInputException {
		String className = null;
		String input = file.nextLine().trim();

		while (!input.equals("//@begin_methods")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("File should start with #@begin_methods");
		}
		
		input = file.nextLine().trim();
		
		while (!input.startsWith("object")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("object name not found");
		}
		className = input.split(" ")[1];

		input = file.nextLine().trim();

		while (!input.equals("//@end_methods")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			if (!input.startsWith("def"))
				throw new BadInputException("methods should start with def"+input);

			List<String> result = new LinkedList<String>();
			while (true) {
				result.add(input);

				input = file.nextLine();
				if (input.trim().startsWith("def") || input.trim().startsWith("//@end_methods")) {
					Method met = new Method(result);
					if (methods.put(met.id(), met) != null)
						throw new BadInputException("repeated method.");
					input = input.trim();
					break;
				}
			}
		}
		
		return className;
		
	}

	private void readImports(Scanner file) throws BadInputException {
		String input = file.nextLine().trim();

		while (!input.equals("//@begin_imports")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			throw new BadInputException("File should start with //@begin_imports");
		}

		input = file.nextLine().trim();

		while (!input.equals("//@end_imports")) {
			if (input.length() == 0) {
				input = file.nextLine().trim();
				continue;
			}
			
			if (!input.startsWith("import"))
				throw new BadInputException("imports should start either with import:" + input);

			ImportScala imp = new ImportScala(input);
			imports.put(imp.toString(), imp);

			input = file.nextLine().trim();
		}
	}

}
