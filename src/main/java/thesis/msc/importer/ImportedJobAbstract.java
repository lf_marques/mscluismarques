package thesis.msc.importer;

import java.util.HashMap;
import java.util.Map;

import thesis.msc.graph.Edge;
import thesis.msc.graph.Graph;
import thesis.msc.graph.VertexSingle;
import thesis.msc.graph.info.Arg;
import thesis.msc.graph.info.Import;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public class ImportedJobAbstract {

	protected static final double DEFAULTWEIGHT = 1.0;
	protected Graph<VertexSingle, Edge> computations;
	protected Map<String, Import> imports;
	protected Map<String, Method> methods;
	protected Map<String, Variable> variables;
	protected Sufix sufix;
	protected String job;

	public ImportedJobAbstract() {
		super();
		imports = new HashMap<String, Import>();
		methods = new HashMap<String, Method>();
		variables = new HashMap<String, Variable>();
	}

	public Graph<VertexSingle, Edge> computationsGraph() {
		return computations;
	}

	public Map<String, Import> imports() {
		return imports;
	}

	public Map<String, Method> methods() {
		return methods;
	}

	public Map<String, Variable> variables() {
		return variables;
	}

	public Sufix sufix() {
		return sufix;
	}

	public String job() {
		return job;
	}
	
	protected class Fields {
		String id;
		String parentId;
		String method;
		Arg arg;

		public Fields(String id, String parentId, String method, Arg arg) {
			this.id = id;
			this.parentId = parentId;
			this.method = method;
			this.arg = arg;
		}
	}
	
	

}