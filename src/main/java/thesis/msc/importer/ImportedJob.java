package thesis.msc.importer;

import java.util.Map;

import thesis.msc.graph.Edge;
import thesis.msc.graph.Graph;
import thesis.msc.graph.VertexSingle;
import thesis.msc.graph.info.Import;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public interface ImportedJob {

	/**
	 * @return the computations graph
	 */
	Graph<VertexSingle, Edge> computationsGraph();

	/**
	 * @return the imports
	 */
	Map<String, Import> imports();

	/**
	 * @return the methods
	 */
	Map<String, Method> methods();

	/**
	 * @return the variables
	 */
	Map<String, Variable> variables();

	/**
	 * @return the sufix, the code that runs after the Spark component
	 */
	Sufix sufix();

	/**
	 * @return the job ID
	 */
	String job();

}