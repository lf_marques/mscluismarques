package thesis.msc.graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import thesis.msc.graph.info.Import;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public abstract class VertexComplexAbstract extends VertexAbstract implements VertexComplex {

	private Map<String, Import> imports;
	private Map<String, Method> methods;
	private Map<String, Variable> variables;
	protected Map<String, Sufix> sufixs;
	private List<String> finalJobName;
	
	public VertexComplexAbstract(double weight, Vertex parent) {
		super(weight, parent);

		this.sufixs = new HashMap<String, Sufix>();
		this.finalJobName = null;
	}
	
	
	
	/**
	 * @return the finalJobName
	 */
	public List<String> getFinalJobName() {
		return finalJobName;
	}



	/**
	 * @param finalJobName the finalJobName to set
	 */
	public void addFinalJobName(String finalJobName) {
		if(this.finalJobName == null)
			this.finalJobName = new LinkedList<String>();
		this.finalJobName.add(finalJobName);
	}
	
	public void addFinalJobName(List<String> finalJobName){
		if(finalJobName == null)
			return;
		if(this.finalJobName == null)
			this.finalJobName = new LinkedList<String>();
		this.finalJobName.addAll(finalJobName);
	}



	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#getImports()
	 */
	public Map<String, Import> getImports() {
		return imports;
	}

	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#addImports(java.util.Map)
	 */
	public void addImports(Map<String, Import> imports) {
		if(this.imports == null)
			this.imports = imports;
		else{
			this.imports.putAll(imports);
		}
			
	}

	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#getMethods()
	 */
	public Map<String, Method> getMethods() {
		return methods;
	}

	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#addMethods(java.util.Map)
	 */
	public void addMethods(Map<String, Method> methods) {
		if(this.methods == null)
			this.methods = methods;
		else{
			this.methods.putAll(methods);
		}
	}

	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#getVariables()
	 */
	public Map<String, Variable> getVariables() {
		return variables;
	}

	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#addVariables(java.util.Map)
	 */
	public void addVariables(Map<String, Variable> variables) {
		if(this.variables == null)
			this.variables = variables;
		else{
			this.variables.putAll(variables);
		}
	}

	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#getSufixs()
	 */
	public Map<String, Sufix> getSufixs() {
		return sufixs;
	}

	/* (non-Javadoc)
	 * @see thesis.msc.graph.Bla#addSufixs(java.lang.String, thesis.msc.graph.info.Sufix)
	 */
	public void addSufixs(String job, Sufix sufix) {
		 sufixs.put(job, sufix);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return super.toString();
		//return "VertexComplexAbstract [imports=" + imports + ", methods=" + methods + ", variables=" + variables
	//			+ ", sufixs=" + sufixs + ", toString()=" + super.toString() + "]";
	}

}
