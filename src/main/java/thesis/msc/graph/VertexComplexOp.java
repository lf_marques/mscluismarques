package thesis.msc.graph;

public class VertexComplexOp extends VertexComplexAbstract implements VertexComplex {

	private VertexSingle operation;
	
	
	public VertexComplexOp(double weight, VertexComplex parent) {
		this(weight, parent, null);
	}

	public VertexComplexOp(double weight, VertexComplex parent, VertexSingle operation) {
		super(weight, parent);
		this.operation = operation;
	}

	

	/**
	 * @return the operation
	 */
	public VertexSingle getOperation() {
		return operation;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((operation == null) ? 0 : operation.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VertexComplexOp other = (VertexComplexOp) obj;
		if (operation == null) {
			if (other.operation != null)
				return false;
		} else if (!operation.equals(other.operation))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return operation.toString();
	}
	
	
	
	

}
