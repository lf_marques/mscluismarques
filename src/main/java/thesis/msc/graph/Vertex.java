package thesis.msc.graph;

public interface Vertex {

	public double getWeight();
	
	public Vertex parent();
	
	@Override
	public int hashCode();
	
	@Override
	public boolean equals(Object obj);
	
}
