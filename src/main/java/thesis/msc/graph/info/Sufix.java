package thesis.msc.graph.info;

import java.util.List;

import thesis.msc.importer.BadInputException;

public class Sufix {

	protected List<String> sufix;
	protected String appName;
	String className;
	String lastRDD;

	public Sufix(List<String> sufix, String appName, String className) throws BadInputException {

		while (true) {
			lastRDD = sufix.remove(sufix.size() - 1).trim();
			if (lastRDD.equals(""))
				continue;
			else if (lastRDD.startsWith("print")) {
				lastRDD = lastRDD.substring(lastRDD.indexOf("(")+1, lastRDD.lastIndexOf(")"));
				break;
			} else
				throw new BadInputException("input of sufix not as expected");
		}

		for(int i=0; i<sufix.size();){
			String line = sufix.get(i).trim();
			
			if(line.equals(""))
				sufix.remove(i);
			else
				i++;
			
		}
		
		this.sufix = sufix;
		this.appName = appName;
		this.className = className;
	}

	public List<String> getSufix() {
		return sufix;
	}

	public String getAppName() {
		return appName;
	}

	public String toString() {
		String result = "";
		for (String s : sufix)
			result += s + "\n";
		return result;
	}

	public String getClassName() {
		return className;
	}

	/**
	 * @return the lastRDD
	 */
	public String getLastRDD() {
		return lastRDD;
	}

}