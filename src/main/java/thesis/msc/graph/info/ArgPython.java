package thesis.msc.graph.info;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

public class ArgPython extends ArgAbstract implements Arg {

	public ArgPython(String input) {
		this.original = input;
		comparable = process(input);
	}

	private String process(String input) {
		String result = input.trim();

		if (result.contains("lambda")) {
			String definition = input.substring("lambda".length() + 1, input.indexOf(':')).trim();
			String rest = input.substring(input.indexOf(':') + 1).trim();

			String a = "var_";
			char c = 'a';

			String[] arr = definition.split("[(),]");
			Map<String, String> newNames = new HashMap<String, String>(arr.length);
			Queue<String> queue = new PriorityQueue<String>(arr.length, new LengthComparator());

			for (String s : arr) {
				s = s.trim();
				if (s.length() == 0)
					continue;
				newNames.put(s, a + c);
				queue.add(s);
				// TODO MAKE THIS STRONGER
				c++;
			}

			while (queue.size() > 0) {
				String s = queue.poll();
				definition = definition.replaceAll(s, newNames.get(s));
				rest = rest.replaceAll(s, newNames.get(s));
			}
			result = "lambda" + "_" + definition + ":" + rest;

			if (!result.contains("\"")) // If there are no Strings remove all
										// white spaces
				result = result.replaceAll("\\s", "");

		}

		return result;

	}

	

	
}
