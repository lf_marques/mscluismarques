package thesis.msc.graph.info;

import java.util.Comparator;

public abstract class ArgAbstract {

	protected String original;
	protected String comparable;

	public ArgAbstract() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comparable == null) ? 0 : comparable.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ArgAbstract other = (ArgAbstract) obj;
		if (comparable == null) {
			if (other.comparable != null)
				return false;
		} else if (!comparable.equals(other.comparable))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return original;
	}

	public class LengthComparator implements Comparator<String> {

		public int compare(String o1, String o2) {
			return o2.length() - o1.length();
		}

	}
}