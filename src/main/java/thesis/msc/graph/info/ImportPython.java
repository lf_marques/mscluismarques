package thesis.msc.graph.info;

import java.util.HashSet;
import java.util.Set;

import thesis.msc.importer.BadInputException;


public class ImportPython extends ImportAbstract implements Import {
	
	private static final int FROMLENGTH = "from".length();
	private String from;
	private boolean importAll;
	private Set<String> imports;
	
	public ImportPython(String line) throws BadInputException{
		int indexOfImport = line.indexOf("import");
		if(indexOfImport < 0)
			throw new BadInputException("imports must contain import keyword: "+line);
		
		imports = new HashSet<String>();
		from = "";
		
		if(indexOfImport > 0){
			from = line.substring(FROMLENGTH,indexOfImport).trim();
		}
		
		String[] listImports = line.substring(indexOfImport+IMPORTLENGTH).split(",");
		
		for(String s: listImports){
			s=s.trim();
			if(s.equals("*")){
				importAll = true;
				imports = null;
				break;
			}
			imports.add(s);
		}
	}
	
	/* (non-Javadoc)
	 * @see thesis.msc.graph.info.Import#mergeImports(thesis.msc.graph.info.ImportPython)
	 */
	public void mergeImports(Import i){
		if(!(i instanceof ImportPython))
			return;
		ImportPython other = (ImportPython)i;
		importAll = importAll || other.importAll;
		if(importAll){
			imports = null;
			return;
		}
		imports.addAll(other.imports);
	}
	
	

	/**
	 * @return the from
	 */
	public String getFrom() {
		return from;
	}

	/**
	 * @return the importAll
	 */
	public boolean isImportAll() {
		return importAll;
	}

	/**
	 * @return the imports
	 */
	public Set<String> getImports() {
		return imports;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	/* (non-Javadoc)
	 * @see thesis.msc.graph.info.Import#toString()
	 */
	@Override
	public String toString() {
		String result = "";
		if(!from.equals(""))
			result = "from "+from+" ";
		
		result+= "import ";
		if(importAll)
			 result += "*";
		else
			result += String.join(",", imports);
		
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	/* (non-Javadoc)
	 * @see thesis.msc.graph.info.Import#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	/* (non-Javadoc)
	 * @see thesis.msc.graph.info.Import#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImportPython other = (ImportPython) obj;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		return true;
	}

}
