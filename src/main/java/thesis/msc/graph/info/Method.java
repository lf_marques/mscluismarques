package thesis.msc.graph.info;

import java.util.List;

public class Method {
	
	private static final int DEFLENGTH = "def".length();
	
	private String id;
	private List<String> method;

	public Method(List<String> method) {
		this.method = method;
		String def = method.get(0);
		this.id = def.substring(DEFLENGTH, def.indexOf('(')).trim();
	}

	/**
	 * @return the id
	 */
	public String id() {
		return id;
	}

	/**
	 * @return the method
	 */
	public List<String> getMethod() {
		return method;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = "";
		for(String s: method)
			result += s+"\n";
		return result;
	}
	
	

}
