package thesis.msc.graph.info;

import thesis.msc.importer.BadInputException;

public class ImportScala extends ImportAbstract implements Import {
	
	private String importLine;

	public ImportScala(String line) throws BadInputException{
		int indexOfImport = line.indexOf("import");
		if(indexOfImport < 0)
			throw new BadInputException("imports must contain import keyword: "+line);
		
		importLine = line.substring(indexOfImport+IMPORTLENGTH).trim();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((importLine == null) ? 0 : importLine.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ImportScala other = (ImportScala) obj;
		if (importLine == null) {
			if (other.importLine != null)
				return false;
		} else if (!importLine.equals(other.importLine))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "import " + importLine;
	}

	

}
