package thesis.msc.graph.info;

public interface Import {

		/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	String toString();

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	int hashCode();

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	boolean equals(Object obj);

}