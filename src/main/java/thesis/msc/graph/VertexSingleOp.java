package thesis.msc.graph;

import thesis.msc.graph.info.Arg;

public class VertexSingleOp extends VertexAbstract implements VertexSingle {

	private Arg argument;
	private String id;
	private String method;
	private String job;
	private boolean isFinal;
	
	public VertexSingleOp(double weight, Vertex parent, Arg argument, String id, String method, String job) {
		super(weight, parent);
		this.argument = argument;
		this.id = id;
		this.method = method;
		this.job = job;
		this.isFinal = true;
	}
	
	

	/**
	 * @return the isFinal
	 */
	public boolean isFinal() {
		return isFinal;
	}



	/**
	 * @param isFinal the isFinal to set
	 */
	public void notFinal() {
		this.isFinal = false;
	}



	public Arg argument() {
		return argument;
	}

	public String id() {
		return id;
	}

	public String method() {
		return method;
	}

	public String job() {
		return job;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((argument == null) ? 0 : argument.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VertexSingleOp other = (VertexSingleOp) obj;
		if (argument == null) {
			if (other.argument != null)
				return false;
		} else if (!argument.equals(other.argument))
			return false;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		return true;
	}

	
	
}
