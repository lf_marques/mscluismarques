package thesis.msc.graph;

import java.util.Set;
import org.jgrapht.WeightedGraph;

public interface Graph<V, E> extends WeightedGraph<V, E> {

	V getRoot();
	
	Set<V> outgoingVertexes(V source);
	
	V getVertex(V equal);
	
}
