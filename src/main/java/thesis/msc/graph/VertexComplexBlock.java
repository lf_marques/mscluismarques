package thesis.msc.graph;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import thesis.msc.graph.info.Sufix;

public class VertexComplexBlock extends VertexComplexAbstract implements VertexComplex {

	private Queue<VertexComplexOp> block;
	private boolean isFinal;
	private boolean continues;
	
	public VertexComplexBlock(double weight, VertexComplex parent) {
		super(weight, parent);
		this.block = new LinkedList<VertexComplexOp>();
		isFinal = false;
		continues = false;
	}

	
	
	/**
	 * @return the isFinal
	 */
	public boolean isFinal() {
		return isFinal;
	}

	/**
	 * @param isFinal the isFinal to set
	 */
	public void setFinal() {
		this.isFinal = true;
	}

	public void setContinues() {
		this.continues = true;
	}

	/**
	 * @return the block
	 */
	public Queue<VertexComplexOp> getBlock() {
		return block;
	}

	/**
	 * @param block the block to set
	 */
	public void addToBlock(VertexComplexOp op) {
		this.block.add(op);
	}

	public void mergeSufixs(Map<String, Sufix> sufixs) {
		if(this.sufixs == null)
			this.sufixs = sufixs;
		else{
			this.sufixs.putAll(sufixs);
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String result = "[";
		for(VertexComplexOp s: block)
			result+= s+", ";
		result += "]";
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((block == null) ? 0 : block.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		VertexComplexBlock other = (VertexComplexBlock) obj;
		if (block == null) {
			if (other.block != null)
				return false;
		} else if (!block.equals(other.block))
			return false;
		return true;
	}



	public boolean continues() {
		return continues;
	}
	
	
	
	


}
