package thesis.msc.graph;

import thesis.msc.graph.info.Arg;

public interface VertexSingle extends Vertex {
	
	public Arg argument();
	public String id();
	public String method();
	public String job();
	public boolean isFinal();
	public void notFinal();

}
