package thesis.msc.graph;

import org.jgrapht.graph.DefaultWeightedEdge;

public class Edge extends DefaultWeightedEdge {

	private static final long serialVersionUID = 1308014547291532702L;
	
	private int jobs;
	
	public Edge(){
		super();
		this.jobs=1;
	}
	
	public void increaseJobs(){
		jobs++;
	}
	
	public int jobs(){
		return jobs;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "(" + getSource() + "=>" + getTarget() + ")";
	}
	
	
	
}
