package thesis.msc.graph;

import java.util.*;

import org.jgrapht.EdgeFactory;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;

public class GraphTree<V, E> extends SimpleDirectedWeightedGraph<V, E>implements Graph<V, E> {

	private static final long serialVersionUID = 5193985075723099154L;
	
	private V root;
	
	public GraphTree(Class<? extends E> edgeClass) {
		this(null, edgeClass);
	}

	public GraphTree(EdgeFactory<V, E> ef) {
		this(null, ef);
	}
	
	public GraphTree(V root, Class<? extends E> edgeClass){
		super(edgeClass);
		initRoot(root);
	}
	
	public GraphTree(V root, EdgeFactory<V, E> ef) {
		super(ef);
		initRoot(root);
	}

	private void initRoot(V root){
		this.addVertex(root);
		this.root=root;
	}
	
	public V getRoot() {
		return root;
	}

	public Set<V> outgoingVertexes(V source) {
		Set<E> edges = this.outgoingEdgesOf(source);
		Set<V> result = new HashSet<V>(edges.size());
		
		for(E e: edges)
			result.add(this.getEdgeTarget(e));

		return result;
	}

	public V getVertex(V equal) {
		V result = null;
		for(V v: this.vertexSet()){
			if(v.equals(equal)){
				result = v;
				break;
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GraphTree [edgeSet()=" + edgeSet() + "]";
	}
	
	
	
	
	
	
	
	

}
