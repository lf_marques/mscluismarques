package thesis.msc.graph;

import java.util.List;
import java.util.Map;

import thesis.msc.graph.info.Import;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public interface VertexComplex extends Vertex {
	
	/**
	 * @return the imports
	 */
	Map<String, Import> getImports();

	/**
	 * @param imports the imports to set
	 */
	void addImports(Map<String, Import> imports);

	/**
	 * @return the methods
	 */
	Map<String, Method> getMethods();

	/**
	 * @param methods the methods to set
	 */
	void addMethods(Map<String, Method> methods);

	/**
	 * @return the variables
	 */
	Map<String, Variable> getVariables();

	/**
	 * @param variables the variables to set
	 */
	void addVariables(Map<String, Variable> variables);

	/**
	 * @return the sufixs
	 */
	Map<String, Sufix> getSufixs();

	/**
	 * @param sufixs the sufixs to set
	 */
	void addSufixs(String job, Sufix sufix);
	
	public List<String> getFinalJobName();

	/**
	 * @param finalJobName the finalJobName to set
	 */
	public void addFinalJobName(String finalJobName);
	
	/**
	 * @param finalJobName the finalJobName to set
	 */
	public void addFinalJobName(List<String> finalJobName);

}
