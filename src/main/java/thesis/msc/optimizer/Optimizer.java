package thesis.msc.optimizer;

import java.util.*;
import thesis.msc.graph.*;
import thesis.msc.importer.*;

public interface Optimizer {
	
	public Graph<VertexComplexOp, Edge> optimize(List<ImportedJob> jobs); 

}
