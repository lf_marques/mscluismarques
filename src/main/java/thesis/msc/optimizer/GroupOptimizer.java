package thesis.msc.optimizer;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import thesis.msc.graph.*;
import thesis.msc.importer.ImportedJob;

public class GroupOptimizer implements Optimizer {

	private static final double DEFAULTWEIGHT = 1.0;

	public GroupOptimizer() {
	}

	public Graph<VertexComplexOp, Edge> optimize(List<ImportedJob> jobs) {
		
		VertexSingleOp sc = new VertexSingleOp(DEFAULTWEIGHT, null, null, "sc", "SparkContext", null);
		VertexComplexOp newRoot = new VertexComplexOp(DEFAULTWEIGHT, null, sc);
		Graph<VertexComplexOp, Edge> result = new GraphTree<VertexComplexOp, Edge>(newRoot, Edge.class);
		
		for(ImportedJob imp : jobs){
			
			Graph<VertexSingle, Edge> computations = imp.computationsGraph();
			

			Queue<Pair> queue = new LinkedList<GroupOptimizer.Pair>();
			for(VertexSingle v : computations.outgoingVertexes(computations.getRoot()))
				queue.add(new Pair(newRoot, v));
			
			while(queue.size()>0){
				Pair pair = queue.poll();
				
				VertexComplexOp newVertex = new VertexComplexOp(DEFAULTWEIGHT, pair.parent, pair.currentVertex);
				VertexComplexOp graphVertex = result.getVertex(newVertex);
				if(graphVertex != null){
					newVertex = graphVertex;
				}
				else{
					result.addVertex(newVertex);
					result.addEdge(pair.parent, newVertex);
				}
				
				if(pair.currentVertex.isFinal())
					newVertex.addFinalJobName(pair.currentVertex.job());
				
				newVertex.addImports(imp.imports());
				newVertex.addMethods(imp.methods());
				newVertex.addVariables(imp.variables());
				newVertex.addSufixs(imp.job(), imp.sufix());

				for(VertexSingle v: computations.outgoingVertexes(pair.currentVertex))
					queue.add(new Pair(newVertex, v));
			}
		}

		return result;
	}
	
	private class Pair{
		
		VertexComplexOp parent;
		VertexSingle currentVertex;
		
		public Pair(VertexComplexOp parent,VertexSingle currentVertex) {
			this.parent = parent;
			this.currentVertex = currentVertex;
		}
		
		
		
	}


}
