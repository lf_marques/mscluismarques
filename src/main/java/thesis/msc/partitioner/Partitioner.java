package thesis.msc.partitioner;


import thesis.msc.graph.*;

public interface Partitioner {
	
	public Graph<VertexComplex, Edge> partitionate(Graph<VertexComplexOp, Edge> optimizedGraph);

}
