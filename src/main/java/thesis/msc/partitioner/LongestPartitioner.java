package thesis.msc.partitioner;

import java.util.Set;
import thesis.msc.graph.*;

public class LongestPartitioner implements Partitioner {

	private static final double DEFAULTWEIGHT = 1.0;
	
	public Graph<VertexComplex, Edge> partitionate(Graph<VertexComplexOp, Edge> optimizedGraph) {
		VertexComplexBlock optRoot = new VertexComplexBlock(DEFAULTWEIGHT, null);
		
		Graph<VertexComplex, Edge> result = new GraphTree<VertexComplex, Edge>(optRoot, Edge.class);
		for (VertexComplexOp v : optimizedGraph.outgoingVertexes(optimizedGraph.getRoot()))
			explore(optimizedGraph, null, optRoot, v, result);
		
		return result;
	}

	private void explore(Graph<VertexComplexOp, Edge> optimizedGraph, VertexComplexBlock opt, VertexComplexBlock parent,
			 VertexComplexOp current, Graph<VertexComplex, Edge> result) {
		if(opt == null){
			opt = new VertexComplexBlock(DEFAULTWEIGHT, parent);
		}
		
		opt.addToBlock(current);
		opt.addFinalJobName(current.getFinalJobName());
		opt.addImports(current.getImports());
		opt.addMethods(current.getMethods());
		opt.addVariables(current.getVariables());
		opt.mergeSufixs(current.getSufixs());
		
		Set<VertexComplexOp> out = optimizedGraph.outgoingVertexes(current);
		if(out.size()==1){
			VertexComplexOp next = out.iterator().next();
			explore(optimizedGraph, opt, parent, next, result);
		}
		else{
			result.addVertex(opt);
			parent.setContinues();
			result.addEdge(parent, opt);
		}
		
		if (out.size() > 1) {
			for(VertexComplexOp next: out)
				explore(optimizedGraph, null, opt, next, result);
		}else if(out.size()==0)
			opt.setFinal();
		
	}
	
	
	
}
