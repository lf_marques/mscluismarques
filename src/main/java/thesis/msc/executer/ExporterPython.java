package thesis.msc.executer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import thesis.msc.graph.VertexComplexOp;
import thesis.msc.graph.VertexSingle;
import thesis.msc.graph.info.Import;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public class ExporterPython implements Exporter {

	PrintableFile file;
	
	public ExporterPython(PrintableFile file) {
		this.file = file;
	}
	
	/* (non-Javadoc)
	 * @see thesis.msc.executer.Exporter#createFile()
	 */
	public File createFile(){

		File f = new File(file.getFilename());
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(f);
		} catch (FileNotFoundException e) {
			return null;
		}

		writeImports(pw, file.getImports());
		pw.println();
		writeMethods(pw, file.getMethods());
		pw.println();
		pw.println("if __name__ == \"__main__\":");
		pw.println();
		writeVariables(pw, file.getVariables());
		pw.println();

		Sufix s = file.getSufixs().get(f.getName());

		if (s != null) {
			writeComputations(pw, s.getAppName(), file.getParentOutput(), file.getOwnOutput(), file.getBlock());
			pw.println();
			writeSufix(pw, s);
		} else
			writeComputations(pw, null, file.getParentOutput(), file.getOwnOutput(), file.getBlock());
		pw.close();

		return f;

	}

	private void writeSufix(PrintWriter pw, Sufix sufix) {
		pw.println(sufix);

	}

	private void writeComputations(PrintWriter pw, String appName, String parentOutput2, String ownOutput2,
			Queue<VertexComplexOp> block2) {

		if(appName == null)
			pw.println("\tsc = SparkContext()");
		else
			pw.println("\tsc = SparkContext(appName=\""+appName+"\")");

		VertexComplexOp b = null;
		String lastID = null;
		if(parentOutput2 != null){
			pw.println("\trdd = sc.textFile(\""+parentOutput2+"\")");
			b = block2.poll();
			VertexSingle op = b.getOperation();
			lastID = op.id();
			pw.println("\t"+op.id()+" = rdd."+op.method()+"("+op.argument()+")");
		}
		
		
		while(block2.size() > 0){
			b = block2.poll();
			VertexSingle op = b.getOperation();
			lastID = op.id();
			pw.println("\t"+op.id()+" = "+op.parent()+"."+op.method()+"("+op.argument()+")");
		}
		
		pw.println("\t"+lastID+".saveAsTextFile(\""+ownOutput2+"\")");

	}

	private void writeVariables(PrintWriter pw, Map<String, Variable> variables2) {
		for (String s : variables2.keySet()) {
			pw.println("\t" + variables2.get(s).toString());
		}

	}

	private void writeMethods(PrintWriter pw, Map<String, Method> methods2) {
		for (String s : methods2.keySet()) {
			pw.println(methods2.get(s).toString());
		}
	}

	private void writeImports(PrintWriter pw, Map<String, Import> imports2) {
		
		Queue<String> imports = new PriorityQueue<String>(imports2.size(), new Comparator<String>() {

			public int compare(String o1, String o2) {
				if(o1.contains("__"))
					return -1;
				if(o2.contains("__"))
					return 1;
				return 0;
			}
		});
		
		for (String s : imports2.keySet()) {
			imports.add(imports2.get(s).toString());
		}
		
		while(imports.size()>0)
			pw.println(imports.poll());
	}

}
