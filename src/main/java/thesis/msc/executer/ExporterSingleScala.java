package thesis.msc.executer;

import java.io.*;
import java.util.*;

import thesis.msc.graph.*;
import thesis.msc.graph.info.*;

public class ExporterSingleScala {

	PrintableSingleFile file;

	public ExporterSingleScala(PrintableSingleFile file) {
		this.file = file;
	}

	public File createFile() {
		File f = new File(file.getFilename());
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(f);
		} catch (FileNotFoundException e) {
			return null;
		}

		// Sufix s = file.getSufixs().get(f.getName());
		String className = randomString(6);

		writeImports(pw, file.getImports());
		pw.println();
		pw.println("object " + className + " {");
		writeMethods(pw, file.getMethods());
		pw.println();
		pw.println("def main(args: Array[String]) {");
		pw.println();
		writeVariables(pw, file.getVariables());
		pw.println();

		writeComputations(pw, file.getBlocks());

		pw.println("}}");

		/*
		 * writeComputations(pw, appName, file.getParentOutput(),
		 * file.getOwnOutput(), file.getBlock());
		 * 
		 * if(s != null){ pw.println(); writeSufix(pw, s); } else
		 * pw.println("}}");
		 */
		pw.close();

		return f;
	}

	private void writeComputations(PrintWriter pw, Queue<VertexComplexBlock> blocks) {
		pw.println("\tval sc = new SparkContext()");

		VertexComplexOp b = null;
		//String lastID = null;

		for (VertexComplexBlock vc : blocks) {

			// pw.println("//Is Final:"+vc.isFinal());
			List<String> sufixsIds = null;

			while (vc.getBlock().size() > 0) {
				sufixsIds = vc.getFinalJobName();
				pw.println();
				b = vc.getBlock().poll();
				VertexSingle op = b.getOperation();
				//lastID = op.id();
				pw.print("\tval " + op.id() + " = " + op.parent() + "." + op.method() + "(" + op.argument() + ")");
			}

			if(vc.continues())
				pw.println(".cache()");
			else
				pw.println();

			
			if (sufixsIds != null) {
				for (String s : sufixsIds) {
					Sufix suf = vc.getSufixs().get(s);
					pw.print(suf);
					
					pw.println("\tnew FileWriter(\""+s+".output.txt\",true) { write("+suf.getLastRDD()+"+\"\\n\"); close }");
				}
			}

			
		}

	}

	private String randomString(int size) {
		char[] arr = "abcdefgijklmnopqrstuwvxyz".toCharArray();
		Random r = new Random();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < size; i++) {
			sb.append(arr[r.nextInt(arr.length)]);
		}
		return sb.toString();
	}

	private void writeVariables(PrintWriter pw, Map<String, Variable> variables) {
		for (String s : variables.keySet()) {
			pw.println("val " + variables.get(s).toString());
		}

	}

	private void writeMethods(PrintWriter pw, Map<String, Method> methods) {
		for (String s : methods.keySet()) {
			pw.println(methods.get(s).toString());
		}
	}

	private void writeImports(PrintWriter pw, Map<String, Import> imports) {
		for (String s : imports.keySet()) {
			pw.println(imports.get(s).toString());
		}
		pw.println("import java.io.{FileWriter}");
		

	}

}
