package thesis.msc.executer;

import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import thesis.msc.graph.*;

public class FilesCreator {

	private int tempCount;
	private String parentFolder;
	private String fileExtension;

	private String getNewTempName() {
		return String.format("tmp_file_%08d", tempCount++);
	}

	public FilesCreator(File f, String fileExtension) {
		tempCount = 0;
		parentFolder = f.getAbsolutePath()+"/";
		this.fileExtension = fileExtension;
	}

	public List<PrintableFile> createFiles(Graph<VertexComplex, Edge> graph) {

		List<PrintableFile> result = new LinkedList<PrintableFile>();

		VertexComplex root = graph.getRoot();
		Queue<Metadata> metadata = new LinkedList<Metadata>();

		populateMetadata(graph, root, metadata, null);

		while (metadata.size() > 0) {
			Metadata next = metadata.poll();
			String filename = next.job;
			if (filename == null)
				filename = getNewTempName()+fileExtension;
			
			String parentOutput = null;
			if(next.parentOutput != null)
				parentOutput = parentFolder+next.parentOutput+ ".output";
			result.add(new PrintableFile(parentFolder+filename, parentFolder+filename + ".output", parentOutput,
					((VertexComplexBlock) next.current).getBlock(), next.current.getImports(),
					next.current.getMethods(), next.current.getVariables(), next.current.getSufixs()));
			populateMetadata(graph, next.current, metadata, filename);
		}

		return result;
	}

	private void populateMetadata(Graph<VertexComplex, Edge> graph, VertexComplex root, Queue<Metadata> metadata,
			String parentOutput) {
		for (VertexComplex v : graph.outgoingVertexes(root)) {
			if (v.getFinalJobName() != null)
				for (String s : v.getFinalJobName())
					metadata.add(new Metadata(parentOutput, v, s));
			else
				metadata.add(new Metadata(parentOutput, v, null));
		}
	}

	private class Metadata {

		String parentOutput;
		VertexComplex current;
		String job;

		public Metadata(String parentOutput, VertexComplex current, String job) {
			this.parentOutput = parentOutput;
			this.current = current;
			this.job = job;
		}

	}
}
