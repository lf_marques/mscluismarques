package thesis.msc.executer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;

import thesis.msc.graph.VertexComplexBlock;
import thesis.msc.graph.VertexComplexOp;
import thesis.msc.graph.VertexSingle;
import thesis.msc.graph.info.Import;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public class ExporterSinglePython {

	PrintableSingleFile file;

	public ExporterSinglePython(PrintableSingleFile file) {
		this.file = file;
	}

	public File createFile() {
		File f = new File(file.getFilename());
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(f);
		} catch (FileNotFoundException e) {
			return null;
		}

		writeImports(pw, file.getImports());
		pw.println();
		writeMethods(pw, file.getMethods());
		pw.println();
		pw.println("if __name__ == \"__main__\":");
		pw.println();
		writeVariables(pw, file.getVariables());
		pw.println();

		writeComputations(pw, file.getBlocks());

		pw.close();

		return f;
	}

	private void writeComputations(PrintWriter pw, Queue<VertexComplexBlock> blocks) {

		pw.println("\tsc = SparkContext()");

		VertexComplexOp b = null;

		for (VertexComplexBlock vc : blocks) {

			List<String> sufixsIds = null;

			while (vc.getBlock().size() > 0) {
				sufixsIds = vc.getFinalJobName();
				pw.println();
				b = vc.getBlock().poll();
				VertexSingle op = b.getOperation();
				pw.print("\t" + op.id() + " = " + op.parent() + "." + op.method() + "(" + op.argument() + ")");
			}
			
			if(vc.continues())
				pw.println(".cache()");
			else
				pw.println("");

			if (sufixsIds != null) {
				for (String s : sufixsIds) {
					Sufix suf = vc.getSufixs().get(s);
					String tab = "\t";
					for(String line: suf.getSufix())
						pw.println(tab+line.trim());
					if(suf.getSufix().size()>0 && 
							suf.getSufix().get(suf.getSufix().size()-1).trim().startsWith("for"))
						tab+="\t";
					pw.println(tab+"with open('" + s + ".output.txt', 'a') as f:");
					pw.println(tab+"\tf.write(str(" + suf.getLastRDD() + ")+\"\\n\")");

				}
			}
		}

	}

	private void writeVariables(PrintWriter pw, Map<String, Variable> variables2) {
		for (String s : variables2.keySet()) {
			pw.println("\t" + variables2.get(s).toString());
		}

	}

	private void writeMethods(PrintWriter pw, Map<String, Method> methods2) {
		for (String s : methods2.keySet()) {
			pw.println(methods2.get(s).toString());
		}
	}

	private void writeImports(PrintWriter pw, Map<String, Import> imports2) {

		Queue<String> imports = new PriorityQueue<String>(imports2.size(), new Comparator<String>() {

			public int compare(String o1, String o2) {
				if (o1.contains("__"))
					return -1;
				if (o2.contains("__"))
					return 1;
				return 0;
			}
		});

		for (String s : imports2.keySet()) {
			imports.add(imports2.get(s).toString());
		}

		while (imports.size() > 0)
			pw.println(imports.poll());
	}

}
