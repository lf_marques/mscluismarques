package thesis.msc.executer;

import java.io.File;
import java.util.*;

import thesis.msc.graph.*;

public class FilesCreatorSingle {

	private String parentFolder;
	private String fileExtension;
	private int tempCount;
	
	public FilesCreatorSingle(File f, String fileExtension) {
		tempCount = 0;
		parentFolder = f.getAbsolutePath()+"/";
		this.fileExtension = fileExtension;
	}
	
	private String getNewTempName() {
		return String.format("opt_file_%08d", tempCount++);
	}

	public List<PrintableSingleFile> createFiles(Graph<VertexComplex, Edge> graph) {

		List<PrintableSingleFile> result = new LinkedList<PrintableSingleFile>();

		VertexComplex root = graph.getRoot();

		for(VertexComplex vc: graph.outgoingVertexes(root)){
			
			PrintableSingleFile file = processBranch((VertexComplexBlock) vc, graph, parentFolder+getNewTempName()+fileExtension);
			result.add(file);
		}
		

		return result;
	}

	private PrintableSingleFile processBranch(VertexComplexBlock vc,Graph<VertexComplex, Edge> graph, String filename) {
		Queue<VertexComplexBlock> queue = new LinkedList<VertexComplexBlock>();
		
		PrintableSingleFile result = new PrintableSingleFile(vc, filename);
		
		for(VertexComplex v: graph.outgoingVertexes(vc))
			queue.add((VertexComplexBlock)v);
		
		
		while(queue.size()>0){
			VertexComplexBlock block = queue.poll();
			result.addBlock(block);
			for(VertexComplex v: graph.outgoingVertexes(block))
				queue.add((VertexComplexBlock)v);
		}
		
		return result;
	}

}
