package thesis.msc.executer;

import java.io.File;

public interface Exporter {

	File createFile();

}