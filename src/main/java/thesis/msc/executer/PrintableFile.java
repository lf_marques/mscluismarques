package thesis.msc.executer;

import java.util.*;

import thesis.msc.graph.*;
import thesis.msc.graph.info.*;

public class PrintableFile {

	private String filename;
	private String ownOutput;
	private String parentOutput;
	private Queue<VertexComplexOp> block;
	private Map<String, Import> imports;
	private Map<String, Method> methods;
	private Map<String, Variable> variables;
	private Map<String, Sufix> sufixs;

	public PrintableFile(String filename, String ownOutput, String parentOutput, Queue<VertexComplexOp> block,
			Map<String, Import> imports, Map<String, Method> methods, Map<String, Variable> variables,
			Map<String, Sufix> sufixs) {
		this.filename = filename;
		this.ownOutput = ownOutput;
		this.parentOutput = parentOutput;
		this.block = block;
		this.imports = imports;
		this.methods = methods;
		this.variables = variables;
		this.sufixs = sufixs;
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @return the ownOutput
	 */
	public String getOwnOutput() {
		return ownOutput;
	}

	/**
	 * @return the parentOutput
	 */
	public String getParentOutput() {
		return parentOutput;
	}

	/**
	 * @return the block
	 */
	public Queue<VertexComplexOp> getBlock() {
		return block;
	}

	/**
	 * @return the imports
	 */
	public Map<String, Import> getImports() {
		return imports;
	}

	/**
	 * @return the methods
	 */
	public Map<String, Method> getMethods() {
		return methods;
	}

	/**
	 * @return the variables
	 */
	public Map<String, Variable> getVariables() {
		return variables;
	}

	/**
	 * @return the sufixs
	 */
	public Map<String, Sufix> getSufixs() {
		return sufixs;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PrintableFile [filename=" + filename + ", ownOutput=" + ownOutput + ", parentOutput=" + parentOutput
				+ "]";
	}
	
	

}
