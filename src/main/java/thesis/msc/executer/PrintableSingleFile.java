package thesis.msc.executer;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

import thesis.msc.graph.*;
import thesis.msc.graph.info.*;

public class PrintableSingleFile {

	private String filename;
	private Queue<VertexComplexBlock> blocks;
	private Map<String, Import> imports;
	private Map<String, Method> methods;
	private Map<String, Variable> variables;
	
	public PrintableSingleFile(VertexComplexBlock vc, String filename) {
		this.filename = filename;
		this.blocks = new LinkedList<VertexComplexBlock>();
		this.blocks.add(vc);
		this.imports = new HashMap<String, Import>(vc.getImports());
		this.methods = new HashMap<String, Method>(vc.getMethods());
		this.variables = new HashMap<String, Variable>(vc.getVariables());
	}
	
	

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @return the block
	 */
	public Queue<VertexComplexBlock> getBlocks() {
		return blocks;
	}

	/**
	 * @param block the block to set
	 */
	public void addBlock(VertexComplexBlock block) {
		this.blocks.add(block);
		mergeImports(block.getImports());
		mergeVariables(block.getVariables());
		mergeMethods(block.getMethods());
	}

	/**
	 * @return the imports
	 */
	public Map<String, Import> getImports() {
		return imports;
	}

	/**
	 * @param imports the imports to set
	 */
	private void mergeImports(Map<String, Import> imports) {
		for(String key: imports.keySet())
			if(!this.imports.containsKey(key))
				this.imports.put(key, imports.get(key));
	}

	/**
	 * @return the methods
	 */
	public Map<String, Method> getMethods() {
		return methods;
	}

	/**
	 * @param methods the methods to set
	 */
	private void mergeMethods(Map<String, Method> methods) {
		for(String key: methods.keySet())
			if(!this.methods.containsKey(key))
				this.methods.put(key, methods.get(key));
	}

	/**
	 * @return the variables
	 */
	public Map<String, Variable> getVariables() {
		return variables;
	}

	/**
	 * @param variables the variables to set
	 */
	private void mergeVariables(Map<String, Variable> variables) {
		this.variables = variables;
	}



	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "PrintableSingleFile [filename=" + filename + ", blocks=" + blocks + ", imports=" + imports
				+ ", methods=" + methods + ", variables=" + variables + "]";
	}

	
	
	
}
