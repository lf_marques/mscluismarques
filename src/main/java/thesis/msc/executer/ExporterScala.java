package thesis.msc.executer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Queue;
import java.util.Random;

import thesis.msc.graph.VertexComplexOp;
import thesis.msc.graph.VertexSingle;
import thesis.msc.graph.info.Import;
import thesis.msc.graph.info.Method;
import thesis.msc.graph.info.Sufix;
import thesis.msc.graph.info.Variable;

public class ExporterScala implements Exporter {

	PrintableFile file;
	
	public ExporterScala(PrintableFile p) {
		this.file = p;
	}

	public File createFile() {
		File f = new File(file.getFilename());
		PrintWriter pw = null;
		try {
			pw = new PrintWriter(f);
		} catch (FileNotFoundException e) {
			return null;
		}

		Sufix s = file.getSufixs().get(f.getName());
		String appName = null;
		String className = null;
		if (s != null) {
			appName = s.getAppName();
			className = s.getClassName();
		}
		else
			className = randomString(6);
		
		
		writeImports(pw, file.getImports());
		pw.println();
		pw.println("object "+className+" {");
		writeMethods(pw, file.getMethods());
		pw.println();
		pw.println("def main(args: Array[String]) {");
		pw.println();
		writeVariables(pw, file.getVariables());
		pw.println();

		

		
		writeComputations(pw, appName, file.getParentOutput(), file.getOwnOutput(), file.getBlock());
			
		if(s != null){
			pw.println();
			writeSufix(pw, s);
		}
		else
			pw.println("}}");
		
		pw.close();

		return f;
	}

	private String randomString(int size) {
		char[] arr = "abcdefgijklmnopqrstuwvxyz".toCharArray();
		Random r = new Random();
		StringBuilder sb = new StringBuilder();
		for(int i = 0; i<size; i++){
			sb.append(arr[r.nextInt(arr.length)]);
		}
		return sb.toString();
	}

	private void writeSufix(PrintWriter pw, Sufix s) {
		pw.println(s);
		
	}

	private void writeComputations(PrintWriter pw, String appName, String parentOutput, String ownOutput,
			Queue<VertexComplexOp> block) {
		if(appName == null)
			pw.println("\tval sc = new SparkContext()");
		else{
			pw.println("\tval conf = new SparkConf().setAppName(\""+appName+"\")");
			pw.println("\tval sc = new SparkContext(conf)");
		}

		VertexComplexOp b = null;
		String lastID = null;
		if(parentOutput != null){
			pw.println("\tval rdd = sc.objectFile(\""+parentOutput+"\")");
			b = block.poll();
			VertexSingle op = b.getOperation();
			lastID = op.id();
			pw.println("\tval "+op.id()+" = rdd."+op.method()+"("+op.argument()+")");
		}
		
		
		while(block.size() > 0){
			b = block.poll();
			VertexSingle op = b.getOperation();
			lastID = op.id();
			pw.println("\tval "+op.id()+" = "+op.parent()+"."+op.method()+"("+op.argument()+")");
		}
		
		pw.println("\t"+lastID+".saveAsObjectFile(\""+ownOutput+"\")");
		
	}

	private void writeVariables(PrintWriter pw, Map<String, Variable> variables) {
		for (String s : variables.keySet()) {
			pw.println("val "+variables.get(s).toString());
		}
		
	}

	private void writeMethods(PrintWriter pw, Map<String, Method> methods) {
		for (String s : methods.keySet()) {
			pw.println(methods.get(s).toString());
		}
	}

	private void writeImports(PrintWriter pw, Map<String, Import> imports) {
		for (String s : imports.keySet()) {
			pw.println(imports.get(s).toString());
		}
		
	}

}
