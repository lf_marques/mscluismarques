package thesis.msc;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import thesis.msc.importer.BadInputException;
import thesis.msc.manager.Manager;

public class App {

	private static final String COMMENT_PYTHON = "#";
	private static final String COMMENT_SCALA = "//";

	public static void main(String[] args) {

		//String[] tmp = {"/Users/lfmarques/Dropbox/ThesisScripts/InputScripts/scriptPython2.txt"};
		//args = tmp;
		
		// Scanner in = new Scanner(System.in);
		Scanner in = null;
		if (args.length == 0)
			in = new Scanner(System.in);
		else {
			try {
				in = new Scanner(new File(args[0]));
			} catch (FileNotFoundException e) {
				System.err.println("BAD_FILE");
				return;
			}
		}

		System.out.print(">Select language of jobs ((S)cala ou (P)ython:\n>");
		String language = in.nextLine().trim().toLowerCase();

		if (args.length != 0)
			System.out.println(language);

		String commentString;
		Manager manager = null;
		if (language.startsWith("s")) {
			manager = new Manager("scala");
			commentString = COMMENT_SCALA;
		} else if (language.startsWith("p")) {
			manager = new Manager("python");
			commentString = COMMENT_PYTHON;
		} else {
			System.err.println("Language not supported.");
			in.close();
			return;
		}

		System.out.print(">");

		while (in.hasNext()) {
			String input = in.nextLine();

			if (args.length != 0)
				System.out.println(input);

			if (input.startsWith(commentString) || input.trim().length() == 0) {
				System.out.print(">");
				continue;
			}

			String[] argums = input.split(" ");
			argums[0] = argums[0].toLowerCase();

			if (argums[0].equals("addjob")) {
				addJob(manager, argums);
			} else if (argums[0].equals("optimize")) {
				System.out.println(manager.print_optimized());
			} else if (argums[0].equals("partitionate")) {
				System.out.println(manager.print_partitionated());
			} else if (argums[0].equals("result")) {
				generateResult(manager, argums);
			}

			System.out.print(">");

		}

		in.close();
	}

	private static void generateResult(Manager manager, String[] argums) {
		if (argums.length != 3) {
			System.err.println("ERROR: arguments");
			System.err.println("EXAMPLE: result single/multi /path/to/Folder");
			return;
		}

		File f = new File(argums[2]);
		if (!f.exists() || !f.isDirectory()) {
			System.err.println("File Not Found");
			return;
		}

		boolean isSingle = false;
		if (argums[1].trim().toLowerCase().equals("single"))
			isSingle = true;
		else if (!argums[1].trim().toLowerCase().equals("multi")) {
			System.err.println("Export method not suported!");
			return;
		}

		try {
			manager.generateResult(f, isSingle);
		} catch (BadInputException e) {
			System.err.println("Language not suported!");
		}

	}

	private static void addJob(Manager manager, String[] argums) {
		if (argums.length != 2) {
			System.err.println("ERROR: arguments");
			System.err.println("EXAMPLE: addjob /path/to/File");
			return;
		}

		File f = new File(argums[1]);
		if (!f.exists()) {
			System.err.println("File Not Found");
			return;
		}

		try {
			manager.processJob(f);
		} catch (FileNotFoundException e) {
			System.err.println("File does not exist!");
		} catch (BadInputException e) {
			System.err.println("Language not suported!");
		}
	}
}
